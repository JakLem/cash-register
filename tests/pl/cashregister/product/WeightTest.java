package pl.cashregister.product;

import static org.junit.Assert.*;
import org.junit.Test;

public class WeightTest {

    @Test
    public void testShouldReturnValueInKilo() {
        Weight weight = new Weight(500);
        assertEquals(0.5, weight.getInKilo(), 0);
    }

    @Test
    public void testShouldCreateWeightObjectByStaticMethodWithWeightInKilo()
    {
        Weight weight = Weight.createByKilo(0.7);
        assertEquals(0.7, weight.getInKilo(), 0);
    }

    @Test(expected = RuntimeException.class)
    public void testShouldThrowAnException()
    {
        Weight weight = new Weight(0);
    }
}
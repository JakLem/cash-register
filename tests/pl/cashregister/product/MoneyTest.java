package pl.cashregister.product;

import org.junit.Test;
import pl.cashregister.shared.Money;

import static org.junit.Assert.*;

public class MoneyTest {
    @Test
    public void testShouldCreateMoneyInstanceWith500Value()
    {
        Money money = new Money(500.0);
        assertEquals(500, money.getValue(), 0);
    }

    @Test(expected = RuntimeException.class)
    public void testShouldThrowAnException()
    {
        Money money = new Money(-0.1);
    }
}

package pl.cashregister.receipt;

import org.junit.Test;
import pl.cashregister.receipt.positions.QuantityPosition;
import pl.cashregister.receipt.positions.WeightPosition;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;
import pl.cashregister.product.Weight;

import static org.junit.Assert.*;

public class ReceiptTest {
    @Test
    public void testShouldSumAllProductsPositions()
    {
        Money price = new Money(200.50);
        Product product = new Product(price, "1234567890");
        Weight weight = new Weight(500);


        WeightPosition weightPosition = new WeightPosition(product, weight);
        QuantityPosition quantityPosition = new QuantityPosition(product, 4);

        Positions productPositions = new Positions();
        productPositions.append(weightPosition);
        productPositions.append(quantityPosition);

        Receipt receipt = new Receipt(productPositions);

        Money receiptCostSum = receipt.calculateSummaryCost();

        assertEquals(902.25, receiptCostSum.getValue(), 0);
    }
}
package pl.cashregister.receipt.positions;

import org.junit.Test;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;

import static org.junit.Assert.*;

public class QuantityPositionTest {
    @Test
    public void testShouldCreateReceiptQuantityPositionWithProperlyPrice()
    {
        Money price = new Money(200.0);
        Product product = new Product(price, "1234567890");

        Position position = new QuantityPosition(product, 2);

        assertEquals(400.0, position.purchasePrice().getValue(), 0);
    }
}
package pl.cashregister.receipt.positions;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;
import pl.cashregister.product.Weight;
import pl.cashregister.receipt.Positions;

public class PositionsTest {

    private Positions positions;

    @Before
    public void setUp()
    {
        Money price = new Money(200.0);
        Product product = new Product(price, "1234567890");
        Weight weight = new Weight(500);


        WeightPosition weightPosition = new WeightPosition(product, weight);
        QuantityPosition quantityPosition = new QuantityPosition(product, 4);

        Positions positions = new Positions();
        positions.append(weightPosition);
        positions.append(quantityPosition);
        
        this.positions = positions;
    }

    @Test
    public void testShouldCreatePositionsListWithTwoPosition()
    {
        Positions positions = this.positions;
        assertEquals(2, positions.count());
    }

    @Test
    public void testShouldIterateAllCollectionAndTakeAllPositions()
    {
        Positions positions = this.positions;

        while (positions.hasNext()) {
            Position position = positions.next();
        }

        assertTrue(positions.isEmpty());
    }

    @Test
    public void testShouldTakeOffFirstOnePositionFromProductsPositions()
    {
        Position position = positions.next();
        assertEquals(WeightPosition.class, position.getClass());

    }

    @Test
    public void testShouldRemoveAllElementsFromPositions()
    {
        Positions positions = this.positions;
        positions.remove();

        assertTrue(positions.isEmpty());
    }
    

}
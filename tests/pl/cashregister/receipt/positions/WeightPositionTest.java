package pl.cashregister.receipt.positions;

import org.junit.Test;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;
import pl.cashregister.product.Weight;

import static org.junit.Assert.*;

public class WeightPositionTest {
    @Test
    public void testShouldCreateReceiptWeightPositionWithProperlyPrice()
    {
        Money price = new Money(200.0);
        Product product = new Product(price, "1234567890");
        Weight weight = new Weight(500);

        Position position = new WeightPosition(product, weight);

        assertEquals(100.0, position.purchasePrice().getValue(), 0);
    }
}
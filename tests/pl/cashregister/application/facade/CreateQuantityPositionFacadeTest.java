package pl.cashregister.application.facade;
import org.junit.Test;
import pl.cashregister.receipt.positions.QuantityPosition;

import static org.junit.Assert.*;
public class CreateQuantityPositionFacadeTest {
    @Test
    public void testShouldCreateQuantityPositionProductByFacade()
    {
        QuantityPosition position = CreateQuantityPositionFacade.create(
                "123456789",
                100.52,
                3
        );

        assertEquals(301.56, position.purchasePrice().getValue(), 0);
    }
}
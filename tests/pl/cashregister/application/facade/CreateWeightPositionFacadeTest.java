package pl.cashregister.application.facade;

import org.junit.Test;
import pl.cashregister.receipt.positions.WeightPosition;

import static org.junit.Assert.*;

public class CreateWeightPositionFacadeTest {
    @Test
    public void testShouldCreateWeightPositionProductByFacade()
    {
        WeightPosition position = CreateWeightPositionFacade.create(
                "123456789",
                129.99,
                1500.0
        );

        assertEquals(194.985, position.purchasePrice().getValue(), 0);
    }
}
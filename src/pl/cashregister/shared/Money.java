package pl.cashregister.shared;

public class Money {
    private Double value;
    private String currency;

    public Money(Double value, String currency)
    {
        this.currency = currency;
        validate(value);
    }

    public Money(Double value)
    {
        validate(value);
        this.currency = "PLN";
        this.value = value;
    }

    private void validate(Double value)
    {
        if (value < 0) {
            throw new RuntimeException("Money value have can't be smaller than 0.");
        }
    }

    public Double getValue() {
        return this.value;
    }

    public String getCurrency() { return currency; }
}

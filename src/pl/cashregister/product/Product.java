package pl.cashregister.product;

import pl.cashregister.shared.Money;

public class Product {
    private final Money price;
    private final String barcode;

    public Product(Money price, String barcode)
    {
        this.price = price;
        this.barcode = barcode;
    }

    public Money getPrice() {
        return price;
    }

    public String getBarcode() {
        return barcode;
    }
}

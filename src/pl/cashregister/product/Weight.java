package pl.cashregister.product;

public class Weight {
    private double value;

    public Weight(double weightInGrams) {
        if (weightInGrams <= 0) {
            throw new RuntimeException("Product weight have to be bigger than 0.");
        }

        this.value = weightInGrams;
    }

    public final double getInKilo()
    {
        return value / 1000;
    }

    public static Weight createByKilo(double weightInKilo)
    {
        return new Weight(weightInKilo * 1_000);
    }
}

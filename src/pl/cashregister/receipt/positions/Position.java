package pl.cashregister.receipt.positions;

import pl.cashregister.shared.Money;

public interface Position{
    public Money purchasePrice();
    public String barcode();
}

package pl.cashregister.receipt.positions;

import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;
import pl.cashregister.product.Weight;

public class WeightPosition implements Position{
    private final Product product;
    private final Weight weight;

    public WeightPosition(Product product, Weight weight)
    {
        this.product = product;
        this.weight = weight;
    }

    public Money purchasePrice()
    {
        var price = product.getPrice().getValue() * weight.getInKilo();
        return new Money(price);
    }

    public String barcode() {
        return this.product.getBarcode();
    }
}

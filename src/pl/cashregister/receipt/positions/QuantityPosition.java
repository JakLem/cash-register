package pl.cashregister.receipt.positions;

import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;

public class QuantityPosition implements Position{
    private final Product product;
    private final int quantity;

    public QuantityPosition(Product product, int quantity)
    {
        this.product = product;
        this.quantity = quantity;
    }

    public Money purchasePrice()
    {
        var price = product.getPrice().getValue() * quantity;
        return new Money(price);
    }

    @Override
    public String barcode() {
        return this.product.getBarcode();
    }
}

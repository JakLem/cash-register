package pl.cashregister.receipt;

import pl.cashregister.shared.Money;
import pl.cashregister.receipt.positions.Position;

public class Receipt {

    final private Positions<Position> positionsOnReceipt;

    public Receipt(Positions<Position> positionsOnReceipt)
    {
        this.positionsOnReceipt = positionsOnReceipt;
    }

    public Money calculateSummaryCost()
    {
        Double sum = this.positionsOnReceipt
                .stream()
                .mapToDouble(p -> p.purchasePrice().getValue())
                .sum()
                ;
        return new Money(sum);
    }
}

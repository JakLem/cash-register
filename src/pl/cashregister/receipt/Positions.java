package pl.cashregister.receipt;

import pl.cashregister.receipt.positions.Position;

import java.util.*;
import java.util.stream.Stream;

public class Positions<T extends Position> implements Iterator<T> {
    private final Queue<T> positions = new ArrayDeque<>();

    public void append(T position)
    {
        this.positions.add(position);
    }

    public boolean isEmpty()
    {
        return positions.isEmpty();
    }

    public Stream<T> stream()
    {
        return this.positions.stream();
    }

    public void remove()
    {
        this.positions.clear();
    }

    public boolean hasNext() {
        return !this.isEmpty();
    }

    public T next() {
        return this.positions.remove();
    }

    public int count()
    {
        return positions.size();
    }

}

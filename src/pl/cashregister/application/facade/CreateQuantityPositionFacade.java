package pl.cashregister.application.facade;

import pl.cashregister.receipt.positions.QuantityPosition;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;

public class CreateQuantityPositionFacade {

    static public QuantityPosition create(
            String barcode, Double priceValue, int quantity
    ) {
        Money price = new Money(priceValue);
        Product product = new Product(price, barcode);

        return new QuantityPosition(product, quantity);
    }

}

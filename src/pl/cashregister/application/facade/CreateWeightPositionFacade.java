package pl.cashregister.application.facade;

import pl.cashregister.receipt.positions.WeightPosition;
import pl.cashregister.shared.Money;
import pl.cashregister.product.Product;
import pl.cashregister.product.Weight;

public class CreateWeightPositionFacade {

    static public WeightPosition create(
            String barcode, Double priceValue, Double weightGramsValue
    ) {
        Money price = new Money(priceValue);
        Product product = new Product(price, barcode);
        Weight weight = new Weight(weightGramsValue);

        return new WeightPosition(product, weight);
    }

}
